import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	login:null,
  	token:null,
  	loading:null
  },
  getters:{
  	getToken(state){
  		return state.token
  	},
  	getLogin(state){
  		return state.login
  	},
  	getLoading(state){
  		return state.loading
  	},
  },
  mutations: {
  	setToken(state, paso) {
            state.token = paso
        },
    setLogin(state, paso) {
            state.login = paso
        },
        setLoading(state, paso) {
            state.loading = paso
        },
  },
  actions: {
  	setToken({
            commit
        }, payload) {
            commit('setToken', payload)
        },
    setLogin({
            commit
        }, payload) {
            commit('setLogin', payload)
        },
        setLoading({
            commit
        }, payload) {
            commit('setLoading', payload)
        },
  }
})
